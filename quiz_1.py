import sys
from PyQt5 import uic
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QApplication, QWidget, QLabel, QInputDialog

GAME_MODE = ""


class Menu(QWidget):
    def __init__(self):
        super().__init__()
        self.name1 = ''
        self.name2 = ''
        self.initUI()

    def initUI(self):
        uic.loadUi('menu.ui', self)
        self.pixmap = QPixmap('image1.png')
        self.image = QLabel(self)
        self.image.move(260, 210)
        self.image.setScaledContents(True)
        self.image.setPixmap(self.pixmap)
        self.image.resize(350, 350)
        self.pixmap1 = QPixmap('image0.png')
        self.image1 = QLabel(self)
        self.image1.move(260, 10)
        self.image1.setScaledContents(True)
        self.image1.setPixmap(self.pixmap1)
        self.image1.resize(350, 150)
        self.pushButton_3.clicked.connect(self.open_rules)
        self.pushButton_2.clicked.connect(self.open_leaderboard)
        self.pushButton.clicked.connect(self.open_dialog1)

    def open_rules(self):
        self.rules = Rules(self, 'help.ui')
        self.rules.show()

    def open_leaderboard(self):
        self.leaderboard = Leaderboard(self, 'leaderboard.ui')
        self.leaderboard.show()

    def open_dialog1(self):
        global GAME_MODE
        self.game_mode, ok_pressed = QInputDialog.getItem(
            self, "Выбор режима игры", "Выберите режим игры",
            ("Один игрок", "Два игрока"), 0, False)
        GAME_MODE = self.game_mode
        if ok_pressed and self.game_mode == 'Один игрок':
            self.name1, ok_pressed = QInputDialog.getText(self, "Ввод имени", "Представьтесь, пожалуйста")
        if ok_pressed and self.game_mode == "Два игрока":
            self.name1, ok_pressed = QInputDialog.getText(self, "Ввод имени", "Игрок1, введите свое имя")
            self.name2, ok_pressed = QInputDialog.getText(self, "Ввод имени", "Игрок2, введите свое имя")


class Rules(QWidget):
    def __init__(self, *args):
        super().__init__()
        self.initUI()

    def initUI(self):
        uic.loadUi('help.ui', self)


class Leaderboard(QWidget):
    def __init__(self, *args):
        super().__init__()
        self.initUI()

    def initUI(self):
        pass


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Menu()
    ex.show()
    sys.exit(app.exec_())
