from random import randint
from threading import Timer
import pandas as pd
from tkinter import *
import sys


class Game:

    categories = ['dinosaurs', 'geography', 'ghibli', 'history', 'literature', 'cosmos']
    used_questions = []

    def __init__(self, category: str):
        self.category = category
        self.data = pd.read_csv(f'Questions/{self.category}.csv')
        self.question_number = self.random_questions()
        self.playing_field_loading()

    # функция генерирует случайный номер вопроса
    def random_questions(self):
        number = randint(0, 29)
        while number in self.used_questions:
            number = randint(0, 29)
        self.used_questions.append(number)
        return number

    def playing_field_loading(self):  # создаем окно игрового поля
        GameWindow(self.category, self.question_number, self.data)


class GameWindow:
    def __init__(self, category, question_number, data):
        self.category = category
        self.question_number = question_number
        self.data = data
        self.numb = 1
        root = Tk()
        if sys.platform != 'linux2':
            root.wm_state('zoomed')
        else:
            root.wm_attributes('-zoomed', True)
        root.resizable(False, False)
        root.configure(background='#fff6e6')
        btn = Button(root, text=self.data['var1'].iloc[self.question_number])
        btn.bind('<Button-1>', self.click)
        btn.place(rely=0.65, relx=0.4, anchor='center', height=150, width=300)
        btn2 = Button(root, text=self.data['var2'].iloc[self.question_number])
        btn2.bind('<Button-1>', self.click)
        btn2.place(rely=0.65, relx=0.6, anchor='center', height=150, width=300)
        btn3 = Button(root, text=self.data['var3'].iloc[self.question_number])
        btn3.bind('<Button-1>', self.click)
        btn3.place(rely=0.85, relx=0.6, anchor='center', height=150, width=300)
        btn4 = Button(root, text=self.data['var4'].iloc[self.question_number])
        btn4.bind('<Button-1>', self.click)
        btn4.place(rely=0.85, relx=0.4, anchor='center', height=150, width=300)
        lbl1 = Label(root, text=self.data[['questions']].iloc[self.question_number].tolist()[0])
        lbl1.place(rely=0.3, relx=0.5, anchor='center', height=200, width=600)
        lbl2 = Label(root, text=f'Вопрос № 1')
        lbl2.place(rely=0.1, relx=0.5, anchor='center', height=50, width=600)
        self.lbl3 = Label(root, background="#fff6e6")
        self.lbl3.place(rely=0.5, relx=0.5, anchor='center', height=50, width=600)
        root.title(self.category)
        root.mainloop()

    def click(self, event):  # будет обрабатывать нажатие на кнопку и выводить верно или нет
        answer = event.widget.cget('text')
        if answer == str(self.data[['correct']].iloc[self.question_number].tolist()[0]):
            self.lbl3.configure(text="Верно!")
        else:
            self.lbl3.configure(text=f'Неверно! Правильный ответ: '
                                     f'{str(self.data[["correct"]].iloc[self.question_number].tolist()[0])}')

        t = Timer(5, self.new_question)
        t.start()

    def new_question(self):  # функция будет перезагружать вопросы и варианты ответа
        self.game_process()

    def game_process(self):
        ...


Game("cosmos")
