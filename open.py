import tkinter as tk  # импортируем tkinter
from PIL import ImageTk, Image   # импортируем pillow для добавления изображений png
from tkinter.ttk import *  # импортируем все методы tkinter
import sys


class Window:

    def __init__(self):
        root = tk.Tk()  # создаем виджет приложения
        width = root.winfo_screenwidth()
        height = root.winfo_screenheight()
        root.geometry("%dx%d" % (width, height))  # устанавливаем размер виджета на весь экран
        root.configure(background='#fff6e6')  # устанавливаем цвет фона
        root.title('гризли')  # добавляем название приложения
        icon = tk.PhotoImage(file="b.png")  # ищем изображение для иконки
        root.iconphoto(True, icon)  # добавляем иконку приложения

        img1 = Image.open('a.png')  # загружаем изображение1 в приложение
        resized_img1 = img1.resize((400, 100))  # меняем его размеры
        new_img1 = ImageTk.PhotoImage(resized_img1)  # сохраняем измененное изображение
        my_label1 = Label(image=new_img1, borderwidth=0, relief='flat', background='#fff6e6')  # убираем границы вокруг
        my_label1.place(relx=0.5, rely=0.1, anchor='center')  # указываем расположение изображения1

        img2 = Image.open('b.png')  # загружаем изображение2 в приложение
        resized_img2 = img2.resize((400, 300))  # меняем его размеры
        new_img2 = ImageTk.PhotoImage(resized_img2)  # сохраняем измененное изображение
        my_label2 = Label(image=new_img2, borderwidth=0, relief='flat', background='#fff6e6')  # убираем границы вокруг
        my_label2.place(relx=0.5, rely=0.4, anchor='center')  # указываем расположение изображения2

        def start():
            root.after(1000, root.destroy)
            root_h = tk.Tk()  # создаем окно для окна выбора количества игроков, темы и дальше
            if sys.platform != 'linux2':
                root_h.wm_state('zoomed')
            else:
                root_h.wm_attributes('-zoomed', True)
            root_h.configure(background='#fff6e6')  # устанавливаем цвет фона
            root_h.title('гризли')  # добавляем название приложения
            root_h.mainloop()
            return

        def table():
            root.after(1000, root.destroy)
            root_h = tk.Tk()  # создаем окно для таблицы рекордов
            width_h = root.winfo_screenwidth()
            height_h = root.winfo_screenheight()
            root_h.geometry("%dx%d" % (width_h, height_h))  # устанавливаем размер окна на весь экран
            root_h.configure(background='#fff6e6')  # устанавливаем цвет фона
            root_h.title('таблица рекордов гризли')  # добавляем название окна
            root_h.mainloop()
            return

        def help_b():
            root_h = tk.Tk()  # создаем окно для помощи в игре
            width_h = root.winfo_screenwidth()
            height_h = root.winfo_screenheight()
            root_h.geometry("%dx%d" % (width_h, height_h))  # устанавливаем размер окна на весь экран
            root_h.configure(background='#fff6e6')  # устанавливаем цвет фона
            root_h.title('помощь от гризли')  # добавляем название окна
            label = Label(root_h, text='Добро пожаловать в гризли квизли!\n\nДля выбора режима игры, нажмите на кнопку '
                                       '"старт" в главном меню. Вы можете играть в игру одни или же вы можете играть '
                                       'вместе с кем-то на одном устройстве.\nПосле выбора режима игры представьтесь '
                                       'Гризли - введите ваше имя или ник, после чего выберете одну из '
                                       'тематик для вашего квиза:\nдинозавры, география, история, гибли, литература, '
                                       'космос.\n\nВ каждой игре вас ждет по 10 вопросов на игрока. Во время игры вы '
                                       'также можете видеть на экране текущий счет. По окончании игры вы можете '
                                       'продолжить\nигру в выбранном режиме или вернуться в главное меню.\n\nДля '
                                       'просмотра рейтинга игроков, которые представились Гризли вашего устройства, '
                                       'нажмите на кнопку "таблица рекордов" в главном меню.',
                          borderwidth=0, relief='flat', font=('Arial', 12), background='#fff6e6')
            label.place(relx=0.5, rely=0.17, anchor="center")
            root_h.mainloop()
            return

        # создаем кнопки
        st = Style()
        st.configure('W.TButton', background='snow2', foreground='black', font=('Arial', 11), borderwidth=0)
        button1 = Button(root, text='старт', style='W.TButton', command=lambda: [root.after(500, root.withdraw()),
                                                                                 start()])  # первая кнопка для старта
        button2 = Button(root, text='таблица рекордов', style='W.TButton', command=lambda:
        [root.after(500, root.withdraw()), table()])  # кнопка2
        button3 = Button(root, text='помощь', style='W.TButton', command=lambda: [root.after(500, root.withdraw()),
                                                                                  help_b()])  # кнопка3 для помощи
        # указываем расположение кнопок
        button1.place(relx=0.5, rely=0.2, anchor="center")
        button2.place(relx=0.5, rely=0.63, anchor="center")
        button3.place(relx=0.5, rely=0.67, anchor="center")

        root.mainloop()  # указываем, что окно приложения не должно закрываться, пока пользователь не сделает этого
